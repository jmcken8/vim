if exists("b:current_syntax")
    finish
endif

syn match new '/new'
syn match pause '/pau \d\+'
syn match cart '==CART=='
syn match comment '#.*'

highlight default link new Operator
highlight default link pause Operator
highlight default link cart String 
highlight default link comment Comment

let b:current_syntax = "frag"

