function! ft#jsfix#JSFix()
    w
    silent !cp % %.bak
    silent !eslint --fix %
    e
    redraw!
endfunction
