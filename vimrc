" local vimrc
if filereadable(expand("$HOME/.vim/vimrc-local"))
    source $HOME/.vim/vimrc-local
endif

" Setup pathogen
call pathogen#infect()
" Needed for Syntax Highlighting and stuff
syntax on
filetype on
filetype plugin indent on

call pathogen#helptags()
" Setup deoplete
" call deoplete#enable()

" Don't auto-complete, too annoying
" call deoplete#custom#option({'auto_complete': v:false})

" Use deoplete completion
" autocmd FileType javascript let g:SuperTabDefaultCompletionType = "<c-x><c-o>"

" Only call deoplete on 'tab'
" inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"

" Only call deoplete on 'tab', v2
" inoremap <silent><expr> <TAB> pumvisible() ? "\<C-n>" :
" "\ <SID>check_back_space() ? "\<TAB>" :
" "\ deoplete#mappings#manual_complete()
" function! s:check_back_space() abort "{{{
" let col = col('.') - 1
" return !col || getline('.')[col - 1]  =~ '\s'
" endfunction"}}}


""""""""""""""""
" Vim settings "
""""""""""""""""

" This shows what you are typing as a command
set showcmd

" Don't redraw until a command is finished (especially useful for that
" function above)

set lazyredraw       

" Allow moving between unsaved buffers

set hidden

" Make wrapping sensible
set wrap
set nolist
set tw=0
set wm=0


" I almost always run in dark

set background=dark
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
colorscheme afterglow

" Who doesn't like autoindent?
set autoindent

" Spaces are better than a tab character
set expandtab
set smarttab

" Who wants an 8 character tab?  Not me!
set shiftwidth=4
set softtabstop=4
set tabstop=4

function TW2()
    exe 'set shiftwidth=2 | set softtabstop=2 | set tabstop=2'
endfunction
command! -nargs=0 TW2 call TW2()

" Make unfortunately needs tabs
autocmd FileType make setlocal noexpandtab

" Cool tab completion stuff
set wildmenu
set wildmode=list:longest,full

" Enable mouse support in console

" Line Numbers are useful
set number

" Ignoring case is a fun trick
set ignorecase

" But only if written in all lowercase
set smartcase

" Speed up Command-T
set wildignore=.o,.obj,.git,node_modules/**,bower_components/** 

" Split on right instead of left

set splitright

" Remap jj and kk to escape in insert mode.  Who types jj or kk anyway??
inoremap jj <Esc>
inoremap kk <Esc>

" In command mode set CTRL-A to the same as every other program - ugh, why is
" this not the default
cnoremap <C-a> <C-b>

" Incremental searching just makes so much more sense
set incsearch

" Highlight things that we find with the search
set hlsearch

"Status line gnarliness
set laststatus=2
set statusline=%F%m%r%h%w\ (%{&ff}){%Y}\ [%l,%v][%p%%]
set statusline+=%#warningmsg#
set statusline+=%*

"Whoop whoop windows
set fileformats=unix,dos
set fileformat=unix

"Backspace? Yeah
set backspace=2

"unix line endings
set ff=unix

set encoding=utf-8

" Space in normal mode -> leader key

nnoremap <SPACE> <Nop>
map <SPACE> <leader>

" Change window with two spaces + direction
nnoremap <Space><Space> <C-w>

" Show open buffers
nnoremap <leader>b :buffers<CR>:buffer<Space>

" Turn off search highlighting with space + j
nnoremap <leader>j :noh<CR>

" Remove whitespace from ends of (select) files
autocmd FileType javascript autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd FileType python autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd FileType cpp autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd FileType se autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd FileType jtse autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd FileType salm autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd FileType cmake autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd FileType make autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd FileType markdown autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd FileType typescript autocmd BufWritePre <buffer> :%s/\s\+$//e

" Look for tags file in current directory and above it
set tags=./tags;,tags;

" Turn sign column off by default. Can be used (for instance) to
" show errors if it were on but also makes copying/pasting harder
set signcolumn=no

"""""""""""""""
" My commands "
"""""""""""""""

" Execute something silently in the background then redraw buffer
command! -nargs=1 Silent execute ':silent !'.<q-args> | execute ':redraw!'

" Read results of command into a new buffer
function! AddToNewBuffer(...)
    let l:arg_string = join(a:000, ' ')
    let l:arg_expanded = expand(l:arg_string)
    "execute ("vnew | setlocal buftype=nofile bufhidden=hide noswapfile | read ! ls")
    execute ("vnew | setlocal buftype=nofile bufhidden=hide noswapfile | read !" . l:arg_expanded)
endfunction
command! -nargs=* -complete=shellcmd R call AddToNewBuffer(<q-args>)

function! ShowGitCachedDiff()
    execute ("vnew | setlocal buftype=nofile bufhidden=hide noswapfile | set syntax=diff | read ! git diff --cached")
endfunction
command! -nargs=0 CDiff call ShowGitCachedDiff()

" Make file completion occur relative to the file being worked on
" Should probably only be done for some file-types... Reevaluate later
function ResetCurrentWorkingDirectory()
    if exists('w:filecompletion_working_directory')
        execute "lcd " . w:filecompletion_working_directory
    endif
endfunction
command ResetCurrentWorkingDirectory call ResetCurrentWorkingDirectory()
autocmd InsertLeave * ResetCurrentWorkingDirectory
inoremap <C-X><C-F> <esc>:let w:filecompletion_working_directory=getcwd()<enter>:lcd %:p:h<enter>a<C-X><C-F>

function! PrintSyntaxStack()
    for id in synstack(line("."), col("."))
        echo synIDattr(id, "name")
    endfor
endfunction

" Format full file as JSON
command! JsonTool %!python3 -m json.tool

" Format selected lines as JSON
"command! -range -nargs=0 -bar JsonTool <line1>,<line2>!python -m json.tool

" Format full file as XML
function! DoPrettyXML()
    " save the filetype so we can restore it later
    let l:origft = &ft
    set ft=
    " delete the xml header if it exists
    1s/<?xml .\{-}?>//e
    " insert fake tags around the entire document
    0put = '<PrettyXML>'
    $put = '</PrettyXML>'
    silent %!xmllint --format -
    2d
    $d
    silent %<
    1
    exe "set ft=" . l:origft
endfunction
command! PrettyXML call DoPrettyXML()

" run current file
nnoremap <leader>r :!%:p<enter>

" Easily set to two instead of four for tabs
function! TW2()
    set shiftwidth=2
    set softtabstop=2
    set tabstop=2
endfunction
command! TW2 call TW2()

"""""""""""""""""""
" Plugin settings "
"""""""""""""""""""

" Disable airline tagbar extension, it's too slow
function! AirlineInit()
  let g:airline#extensions#tagbar#enabled = 0
  let g:airline_powerline_fonts = 1
  let g:airline_section_b = ''
  echom "Called AirlineInit"
endfunction
autocmd User AirlineAfterInit call AirlineInit()

" Open nerdtree
nnoremap <leader>n :NERDTreeToggle<CR>

" Open nerdtree with focus on current file
nnoremap <leader>m :NERDTreeFind<CR>

" Change default width of NERDTree
let g:NERDTreeWinSize = 50

" Open FZF file finder (cmd-t replacement)
nnoremap <leader>t :FZF<CR>

" Python jedi command
let g:jedi#usages_command = "<leader>u"

" toggle ctags view
let g:tagbar_autofocus=1
" stop remapping space key (default option for tagbar)
let g:tagbar_map_showproto="<C-PR>>"
nnoremap <leader>y :TagbarOpenAutoClose<CR>

" delete backwards to first non-space-char .... Doesn't work well, don't use
nnoremap <leader><backspace> ?[^ \r\n\t]<CR>:noh<CR>/\(.\\|\r\\|\n\)<CR>:noh<CR>d/[^ \r\n\t]<CR>:noh<CR>i<backspace><esc>



" Syntastic settings

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_enable_signs=1
" html5
let g:syntastic_html_tidy_exec = 'tidy5'
" debug - temporary while I figure out why syntastic sometimes quits
"let g:syntastic_debug = 1


" This makes command-t work in the pwd rather than the current file's
" directory. Not really using cmd-t anymore (FZF works better)
" but fallback in case FZF isn't installed.
let g:CommandTTraverseSCM = 'pwd'

" Allow command-t to parse more of a directory
let g:CommandTMaxFiles=200000

" Faster implementation of the find algorithm for command-t
let g:CommandTFileScanner="find"

" Speedup Ale by caching executable checks
let g:ale_cache_executable_check_failures = 1

" Jump around errors - two options
nmap <silent> <leader>cn <Plug>(ale_previous_wrap)
nmap <silent> <leader>cp <Plug>(ale_next_wrap)
" ctrl-j and ctrl-k to navigate between Ale errors
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
" Tern (JS) commands
" close preview after selection -- not thaaaat useful
"autocmd FileType javascript autocmd CursorMovedI * if pumvisible() == 0|silent! pclose|endif
" close preview when we enter normal mode however, super useful
autocmd FileType javascript autocmd InsertLeave * if pumvisible() == 0|silent! pclose|endif
autocmd FileType cpp autocmd InsertLeave * if pumvisible() == 0|silent! pclose|endif

" .md file -- need to see on local machine from server
let g:instant_markdown_open_to_the_world = 1


"""""""""""""""""""""
" SoundHound things "
"""""""""""""""""""""

" Treat .ter/.ti/.cdt files as cpp files
au BufNewFile,BufRead *.ter set filetype=cpp
au BufNewFile,BufRead *.ti set filetype=cpp
au BufNewFile,BufRead *.cdt set filetype=cpp

" Auto insert the headers for a file
function InsertHeader()
    let curr_filename=expand('%:t')
    let curr_fileending=expand('%:e')  
    let start_lines = ["/* file \"" . curr_filename . "\" */", "", "/* Copyright ". strftime("%Y") . " SoundHound, Incorporated. All rights reserved. */", ""]
    let end_lines = []
    if curr_fileending == "h" || curr_fileending == "ti"
        let curr_filename=toupper(curr_filename)
        let curr_filename=substitute(curr_filename, "\\.", "_", "")
        call add(start_lines, "#ifndef " . curr_filename)
        call add(start_lines, "#define " . curr_filename)
        call add(start_lines, "")
        call add(end_lines, "#endif /* " . curr_filename . " */")
    endif
    call append(0, start_lines)
    call append(line('$'), end_lines)
endfunction
command InsertHeader call InsertHeader()

function InsertPragma()
    let curr_filename=expand('%:t')
    let curr_fileending=expand('%:e')  
    let start_lines = ["/* file \"" . curr_filename . "\" */", "", "/* Copyright ". strftime("%Y") . " SoundHound, Incorporated. All rights reserved. */", ""]
    let end_lines = []
    if curr_fileending == "h" || curr_fileending == "ti"
        call add(start_lines, "#pragma once")
        call add(start_lines, "")
    endif
    call append(0, start_lines)
    call append(line('$'), end_lines)
endfunction
command InsertPragma call InsertPragma()

" look for a tags hidden file so that ack doesn't always search 'tags'

set tags=.tags;

function SubAll()
   silent %s/a/b/ge
endfunction
command SubAll call SubAll()

" Allow project specific vimrc

set exrc
set secure

" 
let g:svelte_preprocessors = ['typescript']
