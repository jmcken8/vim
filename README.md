# My vim workspace

Clone this repo to ~/.vim/

Link ~/.vimrc to ~/.vim/vimrc

cd ~/.vim

git submodule init
git submodule update --init --recursive

./install

Also requires ctags to be installed if you wish to use ctags features: git@github.com:universal-ctags/ctags.git

Will require a new vim for all features to work (i.e. you get python tips for python2 if vim was built with python2, command-t does not work unless vim was compiled with ruby support)! My personal build configuration for vim.

    ./configure --disable-pythoninterp --enable-python3interp --enable-gui --with-x --with-features=huge --enable-rubyinterp --enable-fail-if-missing --prefix=/home/joel/.local_installs

This will require some dev/devel packages (ruby-dev/devel, etc.), probably going to have to google them.

From my history... Don't think you need all these though.

    dpkg-dev
    curl-devel expat-devel gettext-devel openssl-devel zlib-devel gcc perl-ExtUtils-MakeMaker
    ruby rubygems nodejs npm python-devel
    postgresql-devel
    python34u python34u-devel
    ncurses-devel
    ruby-devel
    libXt-devel libSM-devel libXpm-devel
    libX11-devel
    xorg-dev
    gnome-devel
    build-essential
    cmake
    python-dev
    python3-dev




This requires adding .local_installs to the PATH


-- NPM requirements --
npm install -g eslint

-- PYTHON requirements --
pip3 install jedi
